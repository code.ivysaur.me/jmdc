# jmdc

![](https://img.shields.io/badge/written%20in-Java%20%2F%20J2ME-blue)

An NMDC client for J2ME mobiles.

Never miss another word of hub-chat! jmDC lets you chat and PM from any java phone. Either transfer the .jar file to your phone, or download it directly.

- jmDC requires a J2ME environment compatible with CLDC 1.0 and MIDP 2.0.
- Your phone needs either GPRS, 3G, Wifi or any sort of internet connection to be able to connect to a hub.
- NMDC support is tested working with (at least) PtokaX 0.4.1.2.
- This is a chat-only client for the forseeable future. You may fake a share in order to watch the chat on share-limited hubs.
- If you can't see the entire chat on your screen, press the hash key (#) to hide the top banner.
- Use the left and right arrow keys to change between private message windows and the main chat. You can send a private message by typing /MSG theirNick yourMessage, or by scrolling through the user list.

This project uses code from the GPLv2 jmIRC client http://jmirc.sourceforge.net/ by Juho Vähä-Herttua, from which it also takes its name. Hence, please consider any source files made available in this project to be under the same GPLv2 license.

Tags: nmdc

## See Also

Original thread: http://forums.apexdc.net/topic/4194-jmdc/
Original project page: https://code.google.com/p/jmdc/


## Download

- [⬇️ jmDC_src_1.1.rar](dist-archive/jmDC_src_1.1.rar) *(12.98 KiB)*
- [⬇️ jmDC_src_1.0.rar](dist-archive/jmDC_src_1.0.rar) *(12.88 KiB)*
- [⬇️ jmDC11.jar](dist-archive/jmDC11.jar) *(33.40 KiB)*
- [⬇️ jmDC10.jar](dist-archive/jmDC10.jar) *(33.05 KiB)*
